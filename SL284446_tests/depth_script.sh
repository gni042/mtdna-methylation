#!/bin/bash
for f in `find ./srt_bamfiles/ -name *.srt.bam -exec basename {} \; | sed 's/\.bam//'`; do
	echo "### ${f}"
	echo " view R1"
    samtools-1.9 view -@ 8 -h -L ./bedfile_chrM_chr1.bed ./srt_bamfiles/${f}.bam | \
        awk '$2==99 || $2==83 || $1 ~ /^@/' | \
        samtools-1.9 view -@ 8 -b > ./srt_bamfiles/${f}.R1.bam 

	echo " idx R1"
    samtools-1.9 index -@ 8 ./srt_bamfiles/${f}.R1.bam

	echo " view R2"
    samtools-1.9 view -@ 8 -h -L ./bedfile_chrM_chr1.bed ./srt_bamfiles/${f}.bam | \
        awk '$2==147 || $2==163 || $1 ~ /^@/' | \
        samtools-1.9 view -@ 8 -b > ./srt_bamfiles/${f}.R2.bam 

	echo " idx R2"
    samtools-1.9 index -@ 8 ./srt_bamfiles/${f}.R2.bam

	echo " depth R1"
	samtools-1.9 depth -a -m 0 -b ./bedfile_chrM_chr1.bed ./srt_bamfiles/${f}.R1.bam > ./depth/${f}.R1.txt

	echo " depth R2"
	samtools-1.9 depth -a -m 0 -b ./bedfile_chrM_chr1.bed ./srt_bamfiles/${f}.R2.bam > ./depth/${f}.R2.txt

	echo " depth ALL"
	samtools-1.9 depth -a -m 0 -b ./bedfile_chrM_chr1.bed ./srt_bamfiles/${f}.bam > ./depth/${f}.txt

done
