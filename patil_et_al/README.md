# Re-analysis of Patil et al. dataset

The raw fastq files were assessed using FastQC prior to aligning. Results of
the analyses can be explored in the [qc_1](./qc_1 qc_1) folder. Raw fastq files
where then aligned against the hg38 genome using Bismark. M-bias is calculated
based on the alignments.

Based on the m-bias plots, raw reads are trimmed and FastQC is run on the
resulting trimmed fastq files. Then, reads are aligned to the hg38 genome and
M-bias reassessed. The methylation levels are extracted from these alignments.

