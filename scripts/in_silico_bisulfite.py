#!/usr/bin/env python
import sys, os, traceback, argparse
import re
from Bio import SeqIO
from Bio.Seq import Seq

def main (args):
    input_seq_iter = SeqIO.parse(args.fastq, "fastq")
    for record in input_seq_iter:
        record.seq = Seq(str(record.seq).replace("C","T"))
        SeqIO.write(record, sys.stdout, "fastq")


if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(description='In-silico bisulfite reduction')
        parser.add_argument('fastq', action='store', help="FASTQ file")
        args = parser.parse_args()

        main(args)
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)

