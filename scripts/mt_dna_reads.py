#!/usr/bin/env python
import sys, os, traceback, argparse
import random
import time
import re
from Bio import SeqIO
#from pexpect import run, spawn

def main (args):
    circ_chr = args.circular.split(",")

    chrs = dict()
    for seq_record in SeqIO.parse(args.reference, "fasta"):
        chrs[seq_record.id] = seq_record.seq

    for seq_name in chrs:
        sequence = chrs[seq_name]
        if (seq_name in circ_chr):
            sequence = sequence + sequence[0:args.read_length-1]
        for i in range(0, len(sequence) - args.read_length):
            rseq = sequence[i:i+args.read_length]
            strand = '+'
            sys.stdout.write('@READ:' + str(i) + "\n")
            sys.stdout.write(str(rseq) + "\n")
            sys.stdout.write(strand + "\n")
            sys.stdout.write("F" * args.read_length + "\n")


if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description='Generates reads of a given length starting from each position of the reference choromosomes')
        parser.add_argument('-l', action='store', dest='read_length', default=100, type=int, help="Read length")
        parser.add_argument('-c', action='store', dest='circular', default="", help="Comma-separated circular chromosome names (no spaces!)")
        parser.add_argument('reference', action='store', help="FASTA reference")
        args = parser.parse_args()

        main(args)
        sys.exit(0)

    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)

