#!/usr/bin/env python
import sys, os, traceback, argparse
import re
import pysam
import numpy
#from Bio import SeqIO
#from Bio.Seq import Seq

def main (args):
    samfile = pysam.AlignmentFile(args.bam, "rb")
    sys.stdout.write("position,depth,mean_mapq,sum_mapq\n")
    i = 0
    for position in samfile.pileup(args.reference_name, min_mapping_quality=-1):
        mapqs = position.get_mapping_qualities()
        depth = position.get_num_aligned()
        sys.stdout.write("{:d},{:d},{:.3f},{:d}".format(i+1, depth, numpy.mean(mapqs), sum(mapqs)) + "\n")
        i += 1

    samfile.close()

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(description='Calculate depth and mapping quality from BAM alignment')
        parser.add_argument('reference_name', action='store', help="Reference chromosome name")
        parser.add_argument('bam', action='store', help="BAM file")
        args = parser.parse_args()

        main(args)
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)

