#!/usr/bin/env python
import sys, os, traceback, argparse
import random
import time
import re
from Bio import SeqIO
#from pexpect import run, spawn

def main (args):
    circ_chr = args.circular.split(",")
    print(circ_chr)
    chrs = dict()
    for seq_record in SeqIO.parse(args.reference, "fasta"):
        chrs[seq_record.id] = seq_record.seq

    for seq_name in chrs:
        sequence = chrs[seq_name]
        nreads = int(round(len(sequence) * args.depth / args.read_length))
        if (args.paired):
            for i in range(0, int(round(nreads))):
                rn = random.randrange(0, len(sequence) - (args.read_length*2))
                rseq1 = sequence[rn:rn+args.read_length]
                rseq2 = sequence[rn+args.read_length:rn+(args.read_length*2)]
                strand = ''
                if (random.getrandbits(1)):
                    rseq1 = rseq1.reverse_complement()
                    strand = '+'
                else:
                    rseq2 = rseq2.reverse_complement()
                    strand = '-'
                #TODO: add the mutations
                #TODO: add the circularity
                sys.stdout.write('@RANDOMREAD:' + str(i) + '/1' + "\n")
                sys.stdout.write(str(rseq1) + "\n")
                sys.stdout.write(strand + "\n")
                sys.stdout.write("F" * args.read_length + "\n")
                sys.stdout.write('@RANDOMREAD:' + str(i) + '/2' + "\n")
                sys.stdout.write(str(rseq2) + "\n")
                sys.stdout.write(strand + "\n")
                sys.stdout.write("F" * args.read_length + "\n")
        else:
            for i in range(0, nreads):
                rn = random.randrange(0, len(sequence) - args.read_length)
                rseq = sequence[rn:rn+args.read_length]
                strand = '+'
                if (random.getrandbits(1)):
                    rseq = rseq.reverse_complement()
                    strand = '-'
                #TODO: add the mutations
                #TODO: add the circularity
                sys.stdout.write('@RANDOMREAD:' + str(i) + "\n")
                sys.stdout.write(str(rseq) + "\n")
                sys.stdout.write(strand + "\n")
                sys.stdout.write("F" * args.read_length + "\n")


    # Read reference

if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(description='Generates reads of a given length that uniformly cover the reference choromosome')
        parser.add_argument('-p', action='store_true', dest='paired', help="Paired-end reads")
        parser.add_argument('-l', action='store', dest='read_length', default=100, type=int, help="Read length")
        parser.add_argument('-d', action='store', dest='depth', default=10, type=int, help="Desired sequencing depth")
        parser.add_argument('-e', action='store', dest='seq_error', default=0, type=float, help="Sequencing error")
        parser.add_argument('-c', action='store', dest='circular', default="", help="Comma-separated circular chromosome names (no spaces!)")
        parser.add_argument('reference', action='store', help="FASTA reference")

        args = parser.parse_args()


        #if options.verbose: print(time.asctime())
        main(args)
        #if options.verbose: print(time.asctime())
        #if options.verbose: print('TOTAL TIME IN MINUTES:',(time.time() - start_time) / 60.0)
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)

