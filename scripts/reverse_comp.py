#!/usr/bin/env python
import sys, os, traceback, argparse
import re
from Bio import SeqIO

def main (args):
    input_seq_iter = SeqIO.parse(args.fasta, "fasta")
    for record in input_seq_iter:
        record.seq = record.seq.reverse_complement()
        SeqIO.write(record, sys.stdout, "fasta")


if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(description='Generates reverse complementaries of sequences in FASTA file')
        parser.add_argument('fasta', action='store', help="FASTA file")
        args = parser.parse_args()

        main(args)
        sys.exit(0)
    except KeyboardInterrupt as e: # Ctrl-C
        raise e
    except SystemExit as e: # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)

