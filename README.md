# About this repo

This repository contains several analyses, all related to the methylation of
mitochondrial DNA.


## Mappability of mtDNA changes following bisulfite conversion

The complexity of the DNA sequence is compromised following bisulfite
conversion even in a scenario of a perfect conversion. This arises from the
reduction of the DNA alphabet from 4 letters to 3 letters that some bisulfite
aligners carry to ensure unbiased methylation calls (e.g. Bismark).

Since the two mtDNA strands don't have equal proportions of C bases, it is
conceivable that reads on the L-strand (more abundant in Cs and hence less
complex when all Cs are converted into Ts) are easier to align to nuclear DNA,
resulting in an artificial decrease of depth.

This analysis is executed in the "analysis.Rmd" file, and compiled in the
"analysis.html" document.


## Association between methylation and depth

See folder assoc-meth-depth/


## Depth of chrM and telomere of chr1

See folder SL284446_tests/


## Analysis of Patil et al. data

See folder patil_et_al/ with MultiQC reports.
