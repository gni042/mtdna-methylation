#!/bin/bash

# 1) Generate a fasta file for the other MT dna strand
python3.6 ./scripts/reverse_comp.py ./references/b37_MT.fasta > ./references/b37_MT_rev_comp.fasta

# 2) Generate all possible reads for L and H strands of the MT chromosome
python3.6 ./scripts/mt_dna_reads.py -l 50 -c MT ./references/b37_MT.fasta > ./results/MT_strand1.fastq
python3.6 ./scripts/mt_dna_reads.py -l 50 -c MT ./references/b37_MT_rev_comp.fasta > ./results/MT_strand2.fastq

# 3) Reduce all reads with in-silico bisulfite reaction
python3.6 ./scripts/in_silico_bisulfite.py ./results/MT_strand1.fastq > ./results/MT_strand1_CT.fastq
python3.6 ./scripts/in_silico_bisulfite.py ./results/MT_strand2.fastq > ./results/MT_strand2_CT.fastq

# 4) Align non-converted reads against hg19
bowtie2-2.3.5.1 -x /ref/genomes/b37_decoy/b37_decoy -U ./results/MT_strand1.fastq | samtools-1.9 sort -@ 16 -o ./results/MT_strand1.bam
samtools-1.9 index ./results/MT_strand1.bam
bowtie2-2.3.5.1 -x /ref/genomes/b37_decoy/b37_decoy -U ./results/MT_strand2.fastq | samtools-1.9 sort -@ 16 -o ./results/MT_strand2.bam
samtools-1.9 index ./results/MT_strand2.bam

# 5) Align bisulfite-converted reads against in-silico converted hg19
bowtie2-2.3.5.1 -x /ref/genomes/bisulfite/Bisulfite_Genome/CT_conversion/BS_CT -U ./results/MT_strand1_CT.fastq | samtools-1.9 sort -@ 16 -o ./results/MT_strand1_CT.bam
samtools-1.9 index ./results/MT_strand1_CT.bam
bowtie2-2.3.5.1 -x /ref/genomes/bisulfite/Bisulfite_Genome/GA_conversion/BS_GA -U ./results/MT_strand2_CT.fastq | samtools-1.9 sort -@ 16 -o ./results/MT_strand2_CT.bam
samtools-1.9 index ./results/MT_strand2_CT.bam

# 6) Calculate depth and mapping quality per position
python3.6 ./scripts/assess_dept_qual.py MT ./results/MT_strand1.bam > ./results/MT_strand1_depth.csv
python3.6 ./scripts/assess_dept_qual.py MT ./results/MT_strand2.bam > ./results/MT_strand2_depth.csv
python3.6 ./scripts/assess_dept_qual.py MT_CT_converted ./results/MT_strand1_CT.bam > ./results/MT_strand1_CT_depth.csv
python3.6 ./scripts/assess_dept_qual.py MT_GA_converted ./results/MT_strand2_CT.bam > ./results/MT_strand2_CT_depth.csv



